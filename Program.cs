﻿using System;

namespace exercise18
{
    class Program
    {
        static void Main(string[] args)
        { //Start the program with Clear();
        Console.Clear();
       
        // Task A 

        var num1 = 1;
        var num2 = 2;
        var num3 = 3;
        var num4 = 4;
        var num5 = 5;

        Console.WriteLine($"num1 is {num1}");
        Console.WriteLine($"num2 is {num2}");
        Console.WriteLine($"num3 is {num3}");
        Console.WriteLine($"num4 is {num4}");
        Console.WriteLine($"num5 is {num5}");

        // Task B 
       
        var xum1 = 1;
        var xum2 = 2;
        var xum3 = 3;
        var xum4 = 4;
        var xum5 = 5;
        var xum6 = 6;

        Console.WriteLine($"xum1 + xum2 = {xum1+xum2}");
        Console.WriteLine($"xum3 + xum4 = {xum3+xum4}");
        Console.WriteLine($"xum5 + xum6 = {xum5+xum6}");

        // Task C 

        var hum1 = 1;
        var hum2 = 2;
        var hum3 = 3;
        var hum4 = (hum1+hum2+hum3);

        Console.WriteLine($"hum1+hum2+hum3 = {hum4}");

        //Task D ? uncertainty

        var I = 1;
        var am = 2;
        var thinking = 3;

        Console.WriteLine($"I am thinking multiplied by 3 is equal to {(I+am+thinking)*3}");
        
        
        




        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();

        }
    }
}
